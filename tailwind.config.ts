import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  darkMode: 'class',
  theme: {
    
    screens: {
      '2xl': {'max': '1535px'},

      'xl': {'max': '1280px'},

      'lg': {'max': '1023px'},

      'md': {'max': '767px'},

      'sm': {'max': '639px'},
    },

    extend: {
      display: ["group-hover"],
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },

      colors: {
        'darkest': "rgb(16, 20, 29)",
        "dark-purple": "rgb(3, 1, 16)",
        "code-bg": "rgb(10, 26, 34)",
        'primary': 'rgb(134, 23, 255)',
        "light-purple": "rgb(180, 111, 253)",
        'secondary': 'rgb(69, 255, 205)',
        "blood-red": 'rgb(255, 0, 0)',
      }
    },
  },
  plugins: [],
}
export default config
